const MongoClient = require("mongodb").MongoClient;

let uri;
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
  uri =
    "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false";
} else {
  uri = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0-rp24n.mongodb.net/test?retryWrites=true&w=majority`;
}

const dbo = new Promise((resolve, reject) => {
  MongoClient.connect(
    uri,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, database) => {
      if (err) return reject(err);
      resolve(database.db("playstore"));
    }
  );
});
module.exports = dbo;
