const helpers = require("../helpers");

function forceUpdate(req, res) {
  helpers.handleAppsUpdate();
  res.status(200).send("OK");
}
module.exports = forceUpdate;
