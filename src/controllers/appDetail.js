const dbo = require("../db");

function appDetail(req, res) {
  const appId = req.query.appId;
  dbo.then(db => {
    db.collection("appsInfo").findOne({ appId }, (err, result) => {
      if (err) return console.log(err);
      res.status(200).send(result);
    });
  });
}

module.exports = appDetail;
